Hier soll eine minimalistische Grammatik für die deutsche Sprache beschrieben werden.

# Ziele

* Einfachere Erlernbarkeit von Sprache, vor allem durch Ausländer
* Standardmäßige Geschlechtsneutralität der Wörter, um niemanden versehentlich zu bevorzugen
* Einfaches Verständnis der Sprache durch Computer
* Bevorzugung kurzer Wörter für häufig erwähnte Dinge

# Unterschiede zu deutscher Sprache

* Mengenwörter statt Nomen, Verben und Adjektiven
* Kennzeichnung der grammatischen Struktur durch Partikelwörter statt durch Deklination und Konjugation
* kein grammatisches Geschlecht
* keine grammatische Zeit
* Mengenangaben durch explizite Angaben der Anzahl

## Mengenwörter

Im Deutschen lässt gibt es für nahezu jedes Nomen, Verb und Adjektiv jeweils ein dazugehöriges Wort als eine der anderen Wortarten.

# Beispiele

Mensch, Mensch sein, menschlich

Spieler, spielen, spielend

Schöner, schön sein, schön


## Mengenrelationen

Um ein Objekt zu beschreiben, das Element all dieser Mengen ist, kann man diese vereinen. Dies ist durch hintereinanderschreiben möglich.
`schön spiel mensch` => schöne spielende Menschen

Auch äquivalenz zweier Objekte lässt sich beschreiben und wird durch ein Partikel `ik` gekennzeichnet.
`jung mensch ik kind` => junge Menschen sind Kinder

Implikation lässt sich durch ein anderes Partikel `is` darstellen, und bedeutet einseitige äquivalenz.
`hans is kind` => Hänse sind Kinder (aber Kinder sind auch oft keine Hänse)


## Mengenangaben

Statt über gesamte Mengen zu reden kann man auch über Teilmengen bestimmter Mengen reden.
Dazu wird direkt vor eine Mengenangabe ein Mengenwort geschrieben.
Mengenwörter gibt es folgende (sortiert nach Anzahl):
`jed`: Alle, jeder
`viel`: viele, fast alle
(ohne Partikel): normalerweise
`manch`: manche, ein paar, mittelmäßig viele
`kaum`: kaum ein, (nur) wenige
`ein`: ein (einziger)
`kein`: keine, negation

`ein hans is ein kind` => ein Hans ist ein Kind (so macht der Satz schon mehr Sinn)

`kein `

